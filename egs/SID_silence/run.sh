#!/bin/bash
# Copyright   2021   Tsinghua University (Author: Lantian Li, Yang Zhang)
# Apache 2.0.

SPEAKER_TRAINER_ROOT=../..

train_list_path=data/train_list.csv


loss_type=softmax
scale=30.0
margin=0.2
cuda_device=0

stage=$1
silence_duration=$2

if [ $stage -eq 0 ];then
  # model training
  python local/add_silence.py \
    --duration_len ${silence_duration} \
    --data_list data/test_list.csv \
    --target_data_dir data/sil_${silence_duration} \
    --target_data_list data/sil_${silence_duration}_list.csv
fi

if [ $stage -eq 3 ];then
  # model training
  nnet_type=stru1
  embedding_dim=64
  pooling_type=TMP

  #train_list_path=data/sil_train_a_${silence_duration}_list.csv
  
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --train_list_path $train_list_path \
          --n_mels 80 \
          --max_frames 201 --min_frames 200 \
          --batch_size 64 \
          --num_workers 8 \
          --max_epochs 51 \
          --loss_type $loss_type \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --learning_rate 0.0002 \
          --lr_step_size 5 \
          --lr_gamma 0.40 \
          --margin $margin \
          --scale $scale \
          --eval_interval 5 \
          --eval_frames 0 \
          --apply_metric \
          --save_top_k 30 \
          --distributed_backend dp \
          --reload_dataloaders_every_epoch \
          --gpus 1 \
          --suffix add_audio_mnist
fi

if [ $stage -eq 4 ]; then
  # evaluation in ASP
  pooling_type=ASP
  nnet_type=stru1
  mkdir -p scores/
  # data_dir=data/make_phoneme_data/phoneme_data_new
  #model_dir=exp/stru2_TSP_128_softmax_30.0_0.2add_audio_mnist
  model_dir=stru1/0_ASP
  test_list_path=data/sil_${silence_duration}_list.csv
  #test_list_path=data/test_list.csv
  
  for ckpt_path in "$model_dir"/*; do
    #eiener/nnettraniner/nnetrainer/nnetrainer/nnetrainer/nnetcho $ckpt_path
    #exp $ckpt_path
    #exit 
    score_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).csv
    act_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).pkl
    #nnet_type=$(basename "$ckpt_path" .ckpt)
    #echo $nnet_type
    # 根据nnet_type设置embedd变量
    if [ "$nnet_type" = "stru7" ]; then
      embedding_dim=256
    elif [ "$nnet_type" = "stru8" ]; then
      embedding_dim=512
    else
      embedding_dim=128
    fi
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
            --keep_loss_weight \
            --classification \
            --loss_type $loss_type \
            --checkpoint_path $ckpt_path \
            --eval_frame 200 \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --result_list_path scores/$score_name \
            --act_list_path scores_NoSoftmax/$act_name \
            --nnet_type $nnet_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 8 \
            --gpus 1
 
  #python attn.py
  done
fi


if [ $stage -eq 5 ]; then
  # evaluation in TMP
  pooling_type=TMP
  nnet_type=stru1
  mkdir -p scores/
  # data_dir=data/make_phoneme_data/phoneme_data_new
  model_dir=stru1/0_TMP
  test_list_path=data/sil_${silence_duration}_list.csv
  
  for ckpt_path in "$model_dir"/*; do
    score_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).csv
    act_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).pkl
    #nnet_type=$(basename "$ckpt_path" .ckpt)
    # 根据nnet_type设置embedd变量
    if [ "$nnet_type" = "stru7" ]; then
      embedding_dim=256
    elif [ "$nnet_type" = "stru8" ]; then
      embedding_dim=512
    else
      embedding_dim=64
    fi
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
            --keep_loss_weight \
            --classification \
            --loss_type $loss_type \
            --checkpoint_path $ckpt_path \
            --eval_frame 200 \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --result_list_path scores/$score_name \
            --act_list_path scores_NoSoftmax/$act_name \
            --nnet_type $nnet_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 8 \
            --gpus 1
  done
fi


if [ $stage -eq 6 ]; then
  # evaluation in TSP
  pooling_type=TSP
  nnet_type=stru1
  mkdir -p scores/
  # data_dir=data/make_phoneme_data/phoneme_data_new
  model_dir=stru1/0_TSP
  test_list_path=data/sil_${silence_duration}_list.csv
  
  for ckpt_path in "$model_dir"/*; do
    score_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).csv
    act_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).pkl
    #nnet_type=$(basename "$ckpt_path" .ckpt)
    # 根据nnet_type设置embedd变量
    if [ "$nnet_type" = "stru7" ]; then
      embedding_dim=256
    elif [ "$nnet_type" = "stru8" ]; then
      embedding_dim=512
    else
      embedding_dim=128
    fi
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
            --keep_loss_weight \
            --classification \
            --loss_type $loss_type \
            --checkpoint_path $ckpt_path \
            --eval_frame 200 \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --result_list_path scores/$score_name \
            --act_list_path scores_NoSoftmax/$act_name \
            --nnet_type $nnet_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 8 \
            --gpus 1
  done
fi


if [ $stage -eq 7 ]; then
  # evaluation in TAP
  pooling_type=TAP
  nnet_type=stru1
  mkdir -p scores/
  # data_dir=data/make_phoneme_data/phoneme_data_new
  model_dir=stru1/0_TAP
  test_list_path=data/sil_${silence_duration}_list.csv
  
  for ckpt_path in "$model_dir"/*; do
    score_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).csv
    act_name=$(basename "$test_list_path" .csv)_$(basename "$ckpt_path" .ckpt).pkl
    #nnet_type=$(basename "$ckpt_path" .ckpt)
    # 根据nnet_type设置embedd变量
    if [ "$nnet_type" = "stru7" ]; then
      embedding_dim=256
    elif [ "$nnet_type" = "stru8" ]; then
      embedding_dim=512
    else
      embedding_dim=64
    fi
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
            --keep_loss_weight \
            --classification \
            --loss_type $loss_type \
            --checkpoint_path $ckpt_path \
            --eval_frame 200 \
            --n_mels 80 \
            --train_list_path $train_list_path \
            --test_list_path $test_list_path \
            --result_list_path scores/$score_name \
            --act_list_path scores_NoSoftmax/$act_name \
            --nnet_type $nnet_type \
            --pooling_type $pooling_type \
            --embedding_dim $embedding_dim \
            --scale $scale \
            --margin $margin \
            --num_workers 8 \
            --gpus 1
  done
fi

