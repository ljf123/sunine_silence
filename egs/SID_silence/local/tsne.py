import sys
import os
import numpy as np
import pickle
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import pandas as pd

def load_pkl_from_dir(pkl_dir, data_list):

    df = pd.read_csv(data_list)
    labels = df['utt_spk_int_labels'].tolist()

    # 遍历文件夹中的所有文件
    all_data = []
    # 获取激活文件列表
    activation_list = []
    for file_name in os.listdir(pkl_dir):
        if 'activations' in file_name:
            activation_list.append(file_name)
    get_num = lambda x: int(x.split('.')[0].split('_')[1])
    for file_name in sorted(activation_list, key=get_num):
        # 读取npy文件内容
        file_path = os.path.join(pkl_dir, file_name)
        with open(file_path, 'rb') as f:
            data = pickle.load(f)
            for i in range(len(data)):
                tensor = data[i][0].squeeze()
                # 将张量转换为Python列表
                array = tensor.tolist()
                all_data.append(array)

    #all_data_array = np.array(data)
    all_data_array = np.array(all_data)

    spk2utt = {}
    for i in range(len(all_data_array)):
        if labels[i] not in spk2utt.keys():
            spk2utt[labels[i]] = []
        spk2utt[labels[i]].append(all_data_array[i])


    return spk2utt
def Tsne(spk2data_0, spk2data_1, spk2data_2, title):
    data_0,data_1,data_2,labels = [],[],[],[]
    for key in spk2data_0.keys():
        for i in range(10):
          labels.append(key)
          data_0.append(np.array(spk2data_0[key][i]))
          data_1.append(np.array(spk2data_1[key][i]))
          data_2.append(np.array(spk2data_2[key][i]))

    all_labels = labels * 3
    data = np.concatenate((data_0, data_1, data_2), axis=0)

    print(data.shape)
    tsne = TSNE(n_components=2, init='pca', learning_rate=15, perplexity=10, n_iter=3000)

    transformed_data = tsne.fit_transform(data)

    # plt.figure(figsize=(28, 10))
    # ax1 = plt.subplot(1,3,1)
    plt.figure()
    colors = plt.cm.tab20(np.linspace(0, 1, 60))

    markers_0 = '^'
    markers_1 = 'o'
    markers_2 = 'x'

    # 绘制散点图
    for i in range(len(data)):
        if i < int(len(data)/3):
        #if i < 200:
            plt.scatter(transformed_data[i, 0], transformed_data[i, 1], color=colors[labels[i]], marker=markers_0, label=str(labels[i]))
    #      plt.text(transformed_data[i][0], transformed_data[i][1], all_labels[i], fontsize=10, ha='center', va='bottom')

        elif int(len(data)/3) < i < int(len(data)/3*2):
            plt.scatter(transformed_data[i, 0], transformed_data[i, 1], color=colors[all_labels[i]], marker=markers_1, label=str(all_labels[i]))
        else:
        # if int(len(data)/3*2) < i < int(len(data)/3*2)+200:
            plt.scatter(transformed_data[i, 0], transformed_data[i, 1], color=colors[all_labels[i]], marker=markers_2, label=str(all_labels[i]))

    plt.title(title)
    #plt.legend(loc = "best",fontsize='8')
    plt.savefig('pool_type+.png', bbox_inches='tight')
    import ipdb; ipdb.set_trace()


if __name__ == '__main__':
    data_list_0 = 'data/test_list.csv'
    data_list_4000 = 'data/sil_same_length_test_list.csv'
    data_list_10000 = 'data/sil_10s_list.csv'

    silence_0 = sys.argv[1]
    silence_4000 = sys.argv[2]
    silence_10000 = sys.argv[3]
    pool_type= sys.argv[4]
    spk2data_silence_0 = load_pkl_from_dir(silence_0,data_list_0)
    spk2data_silence_4000 = load_pkl_from_dir(silence_4000,data_list_4000)
    spk2data_silence_10000 = load_pkl_from_dir(silence_10000,data_list_10000)
    
    Tsne(spk2data_silence_0, spk2data_silence_4000, spk2data_silence_10000, pool_type)

