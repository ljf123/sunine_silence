import torch
import pickle
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.stats import linregress


with open('scores_NoSoftmax/sil_0_list_model_ASP.pkl', 'rb') as f:
    data0 = pickle.load(f)
with open('scores_NoSoftmax/sil_01_list_model_ASP.pkl', 'rb') as f:
    data1 = pickle.load(f)
with open('scores_NoSoftmax/sil_02_list_model_ASP.pkl', 'rb') as f:
    data2 = pickle.load(f)

df0 = torch.squeeze(torch.tensor(np.array(data0)))
df1 = torch.squeeze(torch.tensor(np.array(data1)))
df2= torch.squeeze(torch.tensor(np.array(data2)))

# 折线图
x0 = np.arange(1,61)
# 第1个utterance
x1 = df0[0]
y1 = df1[0]
z1 = df2[0]
plt.clf()
plt.figure(figsize=(20, 10))
plt.plot(x0,x1, color='red', label='none slience')
plt.plot(x0,y1, color='blue', label='concatenate 1 times slience')
plt.plot(x0,z1, color='green', label='concatenate 2 times slience')
plt.title('First utterance, target speaker is 46', fontsize=20)
plt.xlabel('speaker id')
plt.xticks(range(1, 61, 1), fontsize=10)
plt.ylabel('score before softmax')
plt.yticks([i/2 for i in range(-12, 17)], fontsize=10)
plt.axhline(y=0, color='black', linestyle='--')
plt.grid(color='lightgray', linestyle='--')
plt.legend()  # 添加图例
plt.savefig('ASP First utterance of 0,1,2.png')
# 第6个utterance
x6 = df0[5]
y6 = df1[5]
z6 = df2[5]
plt.clf()
plt.figure(figsize=(20, 10))
plt.plot(x0,x6, color='red', label='none slience')
plt.plot(x0,y6, color='blue', label='concatenate 1 times slience')
plt.plot(x0,z6, color='green', label='concatenate 2 times slience')
plt.title('Sixth utterance,target speaker is 40', fontsize=20)
plt.xlabel('speaker id')
plt.xticks(range(1, 61, 1), fontsize=10)
plt.ylabel('score before softmax')
plt.yticks([i/2 for i in range(-12, 17)], fontsize=10)
plt.axhline(y=0, color='black', linestyle='--')
plt.grid(color='lightgray', linestyle='--')
plt.legend()  # 添加图例
plt.savefig('ASP Sixth utterance of 0,1,2.png')

# 散点图
x = df0.reshape(-1)
y = df1.reshape(-1)
z = df2.reshape(-1)

plt.clf()
plt.figure(figsize=(15, 10))
plt.scatter(x, y, label='scores before softmax')
slope, intercept, r_value, p_value, std_err = linregress(x, y)  # 进行线性拟合
fit_fn = np.poly1d([slope, intercept])
plt.plot(x, fit_fn(x), color='red', label=f'Linear Fit: {fit_fn}, p={p_value:.3f}')  # 绘制拟合直线
plt.xlabel('none slience')
plt.ylabel('concatenate 1 times slience')
plt.xticks(np.arange(math.ceil(x.min())-1, math.ceil(x.max())+1,10), fontsize=10)
plt.yticks(np.arange(math.ceil(y.min())-1, math.ceil(y.max())+1,10), fontsize=10)
plt.xlim(math.ceil(x.min())-1, math.ceil(x.max())+1)
plt.ylim(math.ceil(y.min())-1, math.ceil(y.max())+1)
plt.axis('equal')
# plt.grid(color='lightgray', linestyle='--')
plt.axvline(x=0, color='black', linestyle='--')
plt.axhline(y=0, color='black', linestyle='--')
plt.legend()
plt.savefig('ASP/ASP 0_1 scatter.png')

plt.clf()
plt.figure(figsize=(15, 10))
plt.scatter(x, z, label='scores before softmax')
slope, intercept, r_value, p_value, std_err = linregress(x, z)  # 进行线性拟合
fit_fn = np.poly1d([slope, intercept])
plt.plot(x, fit_fn(x), color='red', label=f'Linear Fit: {fit_fn}, p={p_value:.3f}')  # 绘制拟合直线
plt.xlabel('none slience')
plt.ylabel('concatenate 2 times slience')
plt.xticks(np.arange(math.ceil(x.min())-1, math.ceil(x.max())+1,10), fontsize=10)
plt.yticks(np.arange(math.ceil(z.min())-1, math.ceil(z.max())+1,10), fontsize=10)
plt.xlim(math.ceil(x.min())-1, math.ceil(x.max())+1)
plt.ylim(math.ceil(z.min())-1, math.ceil(z.max())+1)
plt.axis('equal')
# plt.grid(color='lightgray', linestyle='--')
plt.axvline(x=0, color='black', linestyle='--')
plt.axhline(y=0, color='black', linestyle='--')
plt.legend()
plt.savefig('ASP/ASP 0_2 scatter.png')

plt.clf()
plt.figure(figsize=(15, 10))
plt.scatter(y, z, label='scores before softmax')
slope, intercept, r_value, p_value, std_err = linregress(y, z)  # 进行线性拟合
fit_fn = np.poly1d([slope, intercept])
plt.plot(y, fit_fn(y), color='red', label=f'Linear Fit: {fit_fn}, p={p_value:.3f}')  # 绘制拟合直线
plt.xlabel('concatenate 1 times slience')
plt.ylabel('concatenate 2 times slience')
plt.xticks(np.arange(math.ceil(y.min())-1, math.ceil(y.max())+1,10), fontsize=10)
plt.yticks(np.arange(math.ceil(z.min())-1, math.ceil(z.max())+1,10), fontsize=10)
plt.xlim(math.ceil(y.min())-1, math.ceil(y.max())+1)
plt.ylim(math.ceil(z.min())-1, math.ceil(z.max())+1)
plt.axis('equal')
# plt.grid(color='lightgray', linestyle='--')
plt.axvline(x=0, color='black', linestyle='--')
plt.axhline(y=0, color='black', linestyle='--')
plt.legend()
plt.savefig('ASP/ASP 1_2 scatter.png')

plt.clf()
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x, y, z)
ax.set_xticks(np.arange(math.ceil(x.min())-1, math.ceil(x.max())+1,10))
ax.set_yticks(np.arange(math.ceil(y.min())-1, math.ceil(y.max())+1,10))
ax.set_zticks(np.arange(math.ceil(z.min())-1, math.ceil(z.max())+1,10))
ax.set_xlim(math.ceil(x.min())-1, math.ceil(x.max())+1)
ax.set_ylim(math.ceil(y.min())-1, math.ceil(y.max())+1)
ax.set_zlim(math.ceil(z.min())-1, math.ceil(z.max())+1)
ax.set_xlabel('0 slience')
ax.set_ylabel('concatenate 1 times slience')
ax.set_zlabel('concatenate 1 times slience')
plt.savefig('ASP/ASP 3D Scatter Plot of 0_1_2.png')
