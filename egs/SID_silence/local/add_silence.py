from pydub import AudioSegment
from argparse import ArgumentParser
import os
import pandas as pd
import csv
import numpy as np
import random
def main(duration_len, data_list_path, target_data_dir, target_data_list):
  if not os.path.exists(target_data_dir):
    os.makedirs(target_data_dir)
  df = pd.read_csv(data_list_path)
  data_list = df["utt_paths"].values
  duration_len = float(duration_len)
  for i, file_path in enumerate(data_list):
    filename = os.path.basename(file_path)
    audio = AudioSegment.from_wav(file_path)
    #silent_segment = AudioSegment.silent(duration=duration_len)
    #audio = silent_segment + audio  # 拼接全0静音

    sample_rate = audio.frame_rate
    duration_len = len(audio) * 2
    #duration_len1 = duration_len
    #duration_len1 = random.randint(0,duration_len)  #拼接时长为(0,duration_len)中随机整数ms的静音片段
    #num_samples = int(duration_len1/1000 * sample_rate)
    num_samples = int(duration_len/1000 * sample_rate)
    white_noise = np.random.randn(num_samples) * 0.0001
    white_noise = np.int16(white_noise * 32767)
    silent_segment = AudioSegment(white_noise.tobytes(), frame_rate=sample_rate, sample_width=2, channels=1)

    audio = silent_segment + audio  # 在音频前面拼接静音
    #audio = audio+silent_segment  # 在音频后面拼接静音
    #audio = audio[:int(len(audio) / 2)] + silent_segment + audio[int(len(audio) / 2):]  # 在音频中间拼接静音
    #random_position = random.randint(0, len(audio))
    #audio = audio[:random_position] + silent_segment + audio[random_position:]  #每段语音在随机位置拼接静音
    # audio = audio.overlay(silent_segment, position=int(len(audio)/2))  # 进行叠加，不是拼接    

    output_file = os.path.join(target_data_dir, filename)
    audio.export(output_file, format="wav")

  wav_files = make_four_num_wav_list(target_data_dir, target_data_list)

# 定义函数来遍历文件夹，获取.wav文件的路径
def make_four_num_wav_list(target_data_dir, target_data_list):
    wav_files = []
    for root, dirs, files in os.walk(target_data_dir):
        for file in files:
            if file.endswith(".wav"):
                abs_path = os.path.abspath(os.path.join(root, file))
                wav_files.append(abs_path)


    with open(target_data_list, "w", newline="") as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(['','speaker_name','utt_paths','utt_spk_int_labels'])

        for i, file_path in enumerate(wav_files):
            spk = os.path.basename(file_path).split("_")[0]
            csv_writer.writerow([i, spk, file_path, int(spk)-1])

    return wav_files


if __name__ == "__main__":
    # args
    parser = ArgumentParser()
    parser.add_argument('--duration_len', help='', type=str)
    parser.add_argument('--data_list', help='', type=str)
    parser.add_argument('--target_data_dir', help='', type=str)
    parser.add_argument('--target_data_list', help='', type=str)
    args = parser.parse_args()
    main(args.duration_len, args.data_list, args.target_data_dir, args.target_data_list)
