#!/usr/bin/env python
# encoding: utf-8
import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
try:
    from .pooling_attn import *
except:
    from pooling_attn import *

class TDNN(nn.Module):
    def __init__(self, embedding_dim, pooling_type="TSP", n_mels=80, **kwargs):
        super(TDNN, self).__init__()
        self.layer1 = torch.nn.Conv1d(in_channels=n_mels, out_channels=32, kernel_size=5, stride=1)
        self.bn1 = nn.BatchNorm1d(32)
        self.layer2 = torch.nn.Conv1d(in_channels=32, out_channels=32, kernel_size=3, stride=1)
        self.bn2 = nn.BatchNorm1d(32)
        self.layer3 = torch.nn.Conv1d(in_channels=32, out_channels=64, kernel_size=1, stride=1)
        self.bn3 = nn.BatchNorm1d(64)

        self.fc2 = nn.Linear(128, embedding_dim)

        if pooling_type == "Temporal_Average_Pooling" or pooling_type == "TAP":
            self.pooling = Temporal_Average_Pooling()
            # self.fc1 = nn.Linear(256, 512)
            self.fc1 = nn.Linear(64, 128)

        elif pooling_type == "Temporal_Statistics_Pooling" or pooling_type == "TSP":
            self.pooling = Temporal_Statistics_Pooling()
            # self.fc1 = nn.Linear(64*2, 128)

        elif pooling_type == "Temporal_Max_Pooling" or pooling_type == "TMP":
            self.pooling = Temporal_Max_Pooling()
            self.fc1 = nn.Linear(64, 128)
        
        elif pooling_type == "Attentive_Statistics_Pooling" or pooling_type == "ASP":
            self.pooling = Attentive_Statistics_Pooling(64)
            # self.bn1 = nn.BatchNorm1d(64 * 2)
            self.fc1 = nn.Linear(64*2, 128)
    
        else:
            raise ValueError('{} pooling type is not defined'.format(pooling_type))


    def forward(self, x):
        '''
        x [batch_size, dim, time]
        '''
        x = F.relu(self.layer1(x))
        x = self.bn1(x)

        x = F.relu(self.layer2(x))
        x = self.bn2(x)

        x = F.relu(self.layer3(x))
        x = self.bn3(x)
        
        x,attn = self.pooling(x)

        #with open('attn.pkl', 'wb') as f:
        #    pickle.dump(attn, f)
        # x = F.relu(self.fc1(x))
        # x = self.fc2(x)

        return x,attn


def Speaker_Encoder(embedding_dim=512, **kwargs):
    model = TDNN(embedding_dim, **kwargs)
    return model


if __name__ == '__main__':
    model = Speaker_Encoder()
    total = sum([param.nelement() for param in model.parameters()])
    print(total/1e6)
    data = torch.randn(10, 80, 200)
    out,attn = model(data)
    print(data.shape)
    print(out.shape)
    print(attn.shape)

