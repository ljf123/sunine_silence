#! /usr/bin/python
# -*- encoding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from .utils import accuracy

class LossFunction(nn.Module):
    def __init__(self, embedding_dim, num_classes, **kwargs):
        super(LossFunction, self).__init__()
        self.embedding_dim = embedding_dim
        num_conv_layers = 12 #最后输入的是12 frame
        self.fc_layers = nn.ModuleList([
            nn.Linear(embedding_dim, num_classes)
            for _ in range(num_conv_layers)
        ])

        self.fc = nn.Linear(embedding_dim, num_classes)
        self.criertion = nn.CrossEntropyLoss()

    def forward(self, x, label=None):
        # torch.Size([10, 128, 1, 12])
        x= x.permute(3, 0, 1, 2).contiguous()

        outputs = [fc(x_tmp.squeeze(2)).unsqueeze(2) for fc, x_tmp in zip(self.fc_layers, x)]

        # for j in range(12):
        #     x_tmp = x[:, :, :, j]  # 获取当前位置的张量

        #     outputs = []
        #     for fc in self.fc_layers:
        #         out = fc(x_tmp.squeeze(2)).unsqueeze(2)
        #         outputs.append(out)

        x = torch.cat(outputs, dim=2) 
        x = torch.sum(x, dim=2)

        if label is not None:
          loss = self.criertion(x, label)
          prec1 = accuracy(x.detach(), label.detach(), topk=(1,))[0]
          return loss, prec1
        else:
          return x