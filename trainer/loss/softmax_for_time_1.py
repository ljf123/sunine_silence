#! /usr/bin/python
# -*- encoding: utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from .utils import accuracy

class LossFunction(nn.Module):
    def __init__(self, embedding_dim, num_classes, **kwargs):
        super(LossFunction, self).__init__()
        self.embedding_dim = embedding_dim
   
        self.fc = nn.Linear(embedding_dim, num_classes)
        self.criertion = nn.CrossEntropyLoss()

    def forward(self, x, label=None):
        # torch.Size([10, 128, 1, 12])

        if label is not None:
          x= x.permute(3, 0, 1, 2).contiguous()

          loss = []
          for j in range(10):
              x_tmp = x[j, :, :,0]  # 获取当前位置的张量
              loss.append(self.criertion(self.fc(x_tmp), label))
          loss = torch.sum(torch.stack(loss), dim=0)
          prec1 = accuracy(x_tmp.detach(), label.detach(), topk=(1,))[0]
          return loss, prec1
        else:
          
          x = x.squeeze(0)
          x= x.permute(3, 0, 1, 2).contiguous()
          # ! 要注意啦，这里的x是一个list，里面有12个元素，每个元素是一个tensor
          # ! 手动修改
          x_tmp = x[9, :, :,0]  # 获取当前位置的张量
          return self.fc(x_tmp)
        # if label is not None:
        #   loss = self.criertion(x, label)
        #   prec1 = accuracy(x.detach(), label.detach(), topk=(1,))[0]
        #   return loss, prec1
        # else:
        #   return x
